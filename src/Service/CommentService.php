<?php


namespace App\Service;


use App\Entity\Comment;
use App\Repository\CommentRepository;

class CommentService
{

    /**
     * @var CommentRepository
     */
    private $repository;

    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function addNewComment(Comment $comment)
    {
        $this->repository->save($comment);
    }

    public function getCommentForArticle(\App\Entity\Article $article)
    {
        return $this->repository->findByAricle($article);
    }

}