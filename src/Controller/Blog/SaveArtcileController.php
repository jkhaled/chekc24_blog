<?php


namespace App\Controller\Blog;


use App\Entity\Article;
use App\Entity\User;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class SaveArtcileController extends AbstractController
{

    /**
     * @param ArticleRepository $repository
     *
     * @Route(name="blog.article.create", path="/blog/article", methods={"POST","GET"})
     *
     */
    public function create(
        Request $request,
        ArticleRepository $repository,
        FormFactoryInterface $formFactory,
        UrlGeneratorInterface $urlGenerator
    ): Response {
        $this->denyAccessUnlessGranted(User::ROLE_USER);
        $article = new Article();
        $form = $formFactory->create(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article->setAuthor($this->getUser());
            $repository->save($article);
            return new RedirectResponse($urlGenerator->generate('blog.article.detail',
                ['id' => $article->getId()])
            );
        }
        return $this->render('blog/article_new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

}