<?php


namespace App\Controller\Blog;


use App\Entity\Article;
use App\Entity\Comment;
use App\Entity\User;
use App\Form\CommentType;
use App\Repository\ArticleRepository;
use App\Service\CommentService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ArticleController extends AbstractController
{

    /**
     * @Route(path="/", name="blog.main")
     */
    public function showList(
        Request $request,
        ArticleRepository $articleRepository
    ) {

        $author = $request->get('author');
        if ($author) {
            $articles = $articleRepository->findByAuthor($author);
        }

        $articles = $articleRepository->findAll();

        return $this->render('blog/articles.html.twig', ['articles' => $articles]);
    }

    /**
     * @Route(
     *     name="blog.article.detail",
     *     path="/blog/article/{id}",
     *     methods={"GET", "POST"}
     * )
     * @param Article $article
     * @param FormFactoryInterface $formFactory
     * @param $request
     * @return Response
     */
    public
    function showDetail(
        Article $article,
        Request $request,
        FormFactoryInterface $formFactory,
        CommentService $commentService
    ): Response {

        $comment = new Comment();
        $commentForm = $formFactory->create(CommentType::class, $comment);
        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment->setArticle($article);
            $commentService->addNewComment($comment);
            $commentForm = $formFactory->create(CommentType::class, new Comment());
        }

        $comments = $commentService->getCommentForArticle($article);
        //dd($comments);
        return $this->render('blog/article.html.twig', [
            'article' => $article,
            'comments' => $comments,
            'formComment' => $commentForm->createView(),
        ]);
    }

    /**
     * @Route(path="/cuser", name="create_simple_user")
     */
    public
    function createUser(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoder
    ): Response {
        $user = new User();
        $user->setUsername('check24');
        $user->setPassword($encoder->encodePassword($user, '1234'));
        $em->persist($user);
        $em->flush();
        return new Response("ok");
    }

}