<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Comment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comment[]    findAll()
 * @method Comment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function save(Article $article)
    {
        $this->_em->persist($article);
        $this->_em->flush();
    }

    public function findByAuthor($author)
    {
        return $this->_em->createQueryBuilder()
            ->select('a.title, SUBSTRING(a.content, 0 ,1000]) content, a.createAt')
            ->from($this->_entityName, 'a')
            ->join('a.author', 'au')
            ->andWhere('au.id = :val')
            ->setParameter('val', $author)
            ->orderBy('a.createAt', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function findAllResume()
    {
        return $this->_em->createQueryBuilder()
            ->select('a.title, SUBSTRING(a.content, 10 ), a.createAt, a.author')
            ->from($this->_entityName, 'a')
            ->join('a.author', 'au')
            ->orderBy('a.createAt', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
