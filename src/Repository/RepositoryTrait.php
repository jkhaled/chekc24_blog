<?php


namespace App\Repository;


trait RepositoryTrait
{

    public function save($entity): void
    {
        $this->_em->persist($entity);
        $this->_em->flush();
    }
}